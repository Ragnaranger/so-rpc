# ------------------------------------------------
# Generic Makefile
#
# Author: yanick.rochon@gmail.com
# Date  : 2011-08-10
#
# Changelog :
#   2010-11-05 - first version
#   2011-08-10 - added structure : sources, objects, binaries
#                thanks to http://stackoverflow.com/users/128940/beta
#   2017-04-24 - changed order of linker params
# ------------------------------------------------

# project name (generate executable with this name)
TARGET       = client server
TARGET_CLNT  = client
TARGET_SVC   = server


# change these to proper directories where each file should be
SRCDIR   = src
OBJDIR   = build
BINDIR   = bin
OUTDIR   = out
INCDIR   = $(SRCDIR)

# debug mode: Coloque o valor para 1 para habilitar o modo debug
DEBUG = 0

PFLAGS   = -pedantic -fstack-protector-all

CC       = gcc
# compiling flags here
CFLAGS   = -lm -I $(INCDIR) $(PFLAGS)

LINKER   = gcc
# linking flags here
LFLAGS   = -I $(INCDIR) -lm -ltirpc

# ifeq ($(DEBUG), 1)
# 	CFLAGS += -g -Wall -D DEBUG
# 	LFLAGS += -g -Wall
# endif
rm       = rm -rf


SOURCES_CLNT  := $(shell find $(SRCDIR) -type f -name "*clnt.c")
SOURCES_CLNT  += $(shell find $(SRCDIR) -type f -name "*xdr.c")
INCLUDES      := $(shell find $(SRCDIR) -type f -name "*.h")
OBJECTS_CLNT  := $(SOURCES_CLNT:$(SRCDIR)/%.c=$(OBJDIR)/%.o)


SOURCES_SVC  := $(shell find $(SRCDIR) -type f -name "*svc.c")
SOURCES_SVC  += $(shell find $(SRCDIR) -type f -name "*xdr.c")
INCLUDES     := $(shell find $(SRCDIR) -type f -name "*.h")
OBJECTS_SVC  := $(SOURCES_SVC:$(SRCDIR)/%.c=$(OBJDIR)/%.o)



all: client server

.PHONY: client
client: client

	@echo "--------dentro de client---------"

$(BINDIR)/$(TARGET_CLNT): $(OBJECTS_CLNT) $(INCLUDES)
	mkdir -p $(dir $@)
	$(LINKER) $(OBJECTS_CLNT) $(LFLAGS) -o $@
	@echo "Linking complete!"

$(OBJECTS_CLNT): $(OBJDIR)/%.o : $(SRCDIR)/%.c $(SRCDIR)
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled \"$<\" successfully!"



.PHONY: server
server: server

	@echo "--------dentro de server---------"

$(BINDIR)/$(TARGET_SVC): $(OBJECTS_SVC) $(INCLUDES)
	mkdir -p $(dir $@)
	$(LINKER) $(OBJECTS_SVC) $(LFLAGS) -o $@
	@echo "Linking complete!"

$(OBJECTS_SVC): $(OBJDIR)/%.o : $(SRCDIR)/%.c $(SRCDIR)
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@
	@echo "Compiled \"$<\" successfully!"



.PHONY: clean
clean:
	$(rm) $(OBJECTS_CLNT)
	$(rm) $(OBJECTS_SVC)

	@echo "Cleanup complete!"

.PHONY: remove
remove: clean
	$(rm) $(BINDIR)/$(TARGET_CLNT)
	$(rm) $(BINDIR)/$(TARGET_SVC)
	@echo "Executable removed!"

remake: remove $(BINDIR)/$(TARGET_CLNT) $(BINDIR)/$(TARGET_SVC)
	@echo "Remaked!"
